<?php

namespace App;

use App\Core\Traits\DateTrait;
use App\Notifications\CustomResetPassword;
use App\Core\Eloquent\DatabaseNotification;
use App\Core\Traits\HasDatabaseNotifications;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\RoutesNotifications;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class User extends Authenticatable
{
    use HasRolesAndAbilities, SoftDeletes,
    DateTrait,HasDatabaseNotifications, RoutesNotifications;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 'created_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'admin' => 'boolean',
    ];

    public function isAdmin()
    {
        return false;
    }

    /**
     * Funcion para resetear clave de usuario.
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPassword($token));
    }

    public function notifications()
    {
        return $this->morphMany(DatabaseNotification::class, 'notifiable')->orderBy('created_at', 'desc');
    }

    public function scopeName($query, $name)
    {
        return $query->where('name', 'like', "%$name%");
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public static function boot()
    {
        static::creating(function ($model) {
            $model->created_by = currentUser()->id;
        });

        static::deleting(function ($model) {
        });

        parent::boot();
    }

    public function createdUser()
    {
        return $this->belongsTo(Admin::class, 'created_by');
    }
}
