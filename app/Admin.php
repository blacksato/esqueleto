<?php

namespace App;

use App\Core\Traits\DateTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\Core\Eloquent\DatabaseNotification;
use App\Core\Traits\HasDatabaseNotifications;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\RoutesNotifications;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Hash;

class Admin extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasRolesAndAbilities,SoftDeletes,
    Authenticatable, Authorizable, DateTrait,HasDatabaseNotifications, RoutesNotifications;

    public function isAdmin()
    {
        return true;
    }

    public function notifications()
    {
        return $this->morphMany(DatabaseNotification::class, 'notifiable')->orderBy('created_at', 'desc');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 'created_by',
    ];

    public function scopeName($query, $name)
    {
        return $query->where('name', 'like', "%$name%");
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public static function boot()
    {
        static::creating(function ($model) {
            $model->created_by = currentUser()->id;
        });

        static::deleting(function ($model) {
        });

        parent::boot();
    }

    public function createdUser()
    {
        return $this->belongsTo(Admin::class, 'created_by');
    }
}
