<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactMailNotification extends Notification implements ShouldQueue
{
    use Queueable;
    private $parameters;
    private $clientNotfication;

    /**
     * Create a new notification instance.
     */
    public function __construct($clientNotfication, $parameters)
    {
        $this->parameters = $parameters;
        $this->clientNotfication = $clientNotfication;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(__('Notifications System'))
            ->greeting(__('Greeting'))
            ->line(__('labels.name').': '.$this->clientNotfication->name)
            ->line(__('labels.emailContact').': '.$this->clientNotfication->email)
            ->line(__('labels.description').': '.$this->parameters['contactDescription'])
            ->action(__('Login'), route('admin.login'))
            ->salutation(__('Regard').config('app.name'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->clientNotfication->id,
            'name' => $this->clientNotfication->name,
            'description' => $this->parameters['contactDescription'],
            'redirect_url' => route('home'),        ];
    }
}
