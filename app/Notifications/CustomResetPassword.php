<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword;

class CustomResetPassword extends ResetPassword
{
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(__('Recovery Password'))
            ->greeting(__('Greeting'))
            ->line(__('RecoveryPasswordMessage'))
            ->action(__('Recovery Password'), route('password.reset', $this->token))
            ->line(__('DiscartActionNotification'))
            ->salutation(__('Regard').config('app.name'));
    }
}
