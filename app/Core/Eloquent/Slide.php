<?php

namespace App\Core\Eloquent;

use App\Core\Traits\DateTrait;
use App\Core\Traits\AuditTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slide extends Model
{
    use DateTrait,SoftDeletes,AuditTrait;
    protected $fillable = [
        'vpath', 'position', 'url',
    ];

    public function setVpathAttribute($vpath)
    {
        if (!empty($vpath)) {
            $name = uniqid().'.'.$vpath->getClientOriginalExtension();
            if (!empty($this->vpath)) {
                Storage::disk('public')->delete($this->vpath);
            }
            $this->attributes['vpath'] = $name;
            Storage::disk('public')->put($name, \File::get($vpath));
        }
    }

    public function scopeUrl($query, $url)
    {
        return $query->where('url', 'like', "%$url%");
    }
}
