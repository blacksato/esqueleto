<?php

namespace App\Core\Eloquent\Security;

use App\Core\Traits\DateTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Menu extends Model
{
    use DateTrait,SoftDeletes;

    protected $fillable = ['name', 'route', 'parent', 'order', 'icon'];

    public function getChildren(&$data, $line)
    {
        $children = [];
        foreach ($data as $item) {
            if ($line['id'] == $item['parent']) {
                $children = array_merge($children, [array_merge($item, ['submenu' => $this->getChildren($data, $item)])]);
            }
        }

        return $children;
    }

    public function optionsMenu($rolesID, $relation = 'leftJoin')
    {
        $data = $this
        ->$relation('role_menus as OP', function ($join) use (&$rolesID) {
            $join->on('menus.id', '=', 'OP.menu_id')
                ->whereIn('OP.role_id', $rolesID);
        })->select('menus.order','menus.id', 'menus.name',
            'menus.route', DB::raw('ISNULL(menus.parent,0) as parent'),
            'OP.menu_id AS check',
            'menus.icon AS icons')->distinct()
            ->get()->toArray();

        (usort($data, build_sorter('order')));

        return $data;
    }

    public static function menus($rolesID, $relation = 'leftJoin')
    {
        $menus = new Menu();
        $data = $menus->optionsMenu($rolesID, $relation);
        $menuAll = [];
        foreach ($data as $line) {
            $item = [array_merge($line, ['submenu' => $menus->getChildren($data, $line)])];
            $menuAll = array_merge($menuAll, $item);
        }

        return $menus->menuAll = array_filter($menuAll, function ($item) {
            return $item['parent'] == 0;
        });
    }

    public function scopeName($query, $name)
    {
        return $query->where('name', 'like', "%$name%");
    }
}
