<?php

namespace App\Core\Contracts;

use DB;
use App\User;
use App\Admin;

class TableRepository
{
    public function forMenus($scope)
    {
        $scope = $scope == null ? '' : $scope;

        return DB::table('menus as a')
            ->leftJoin('menus as b', 'b.id', '=', 'a.parent')
            ->where('a.name', 'LIKE', '%'.$scope.'%')
            ->whereNull('a.deleted_at')
            ->orderBy('a.name', 'desc')
            ->select('a.*', 'b.name as father')
            ->paginate(15)->appends('filter', $scope);
    }

    public function forUsers($scope)
    {
        return User::name($scope)->paginate()->appends('filter', $scope);
    }

    public function forAdmins($scope)
    {
        return Admin::name($scope)->paginate()->appends('filter', $scope);
    }
}
