<?php

/**
 * Created by PhpStorm.
 * User: blacksato
 * Date: 8/1/2018
 * Time: 19:19.
 */

namespace App\Core\Traits;

trait AuditTrait
{
    public static function boot()
    {
        static::creating(function ($model) {
            $model->created_by = currentUser()->id;
            $model->created_ip = request()->ip();
            $model->updated_by = currentUser()->id;
            $model->updated_ip = request()->ip();
        });

        static::updating(function ($model) {
            $model->updated_by = currentUser()->id;
            $model->updated_ip = request()->ip();
        });

        static::deleting(function ($model) {
        });

        parent::boot();
    }
}
