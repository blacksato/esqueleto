<?php

/**
 * Created by PhpStorm.
 * User: blacksato
 * Date: 8/1/2018
 * Time: 19:19
 */

namespace App\Core\Traits;

trait DateTrait
{

    public function getDateFormat()
    {
        return getFormatDateSQL(true, true);
    }
}