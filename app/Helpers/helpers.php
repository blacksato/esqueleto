<?php

function currentUser()
{
    return auth()->user();
}

function getMonthNamebyId($id)
{
    $month = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

    return $month[$id];
}

function getFormatDateSQL($date = true, $time = true)
{
    if ($date && $time) {
        if (PHP_OS == 'Linux') {
            return 'Y-m-d H:i:s';
        } else {
            return 'd/m/Y H:i:s';
        }
    } elseif ($date) {
        if (PHP_OS == 'Linux') {
            return 'Y-m-d';
        } else {
            return 'd/m/Y';
        }
    } else {
        return 'H:i:s';
    }
}

function sipUpper($string)
{
    return mb_strtoupper($string, 'UTF-8');
}

function getYear()
{
    return Carbon\Carbon::now()->format('Y');
}

function strMaskData($string, $max = 20, $mask = '...')
{
    $max = strlen($string) > $max ? $max : strlen($string);

    return str_pad(substr($string, 0, $max), $max + 3, $mask);
}

function getDateSQL($date = true, $time = true)
{
    return date(getFormatDateSQL($date, $time));
}

function autorizePutPost($method, $ability)
{
    if ($method == 'POST') {
        return currentUser()->can('create-' . $ability);
    }
    if ($method == 'PUT') {
        return currentUser()->can('update-' . $ability);
    }

    return false;
}

function build_sorter($clave)
{
    return function ($a, $b) use ($clave) {
        return strnatcmp($a[$clave], $b[$clave]);
    };
}

function getInfoServer()
{
    $resultData = [];
    if (PHP_OS == 'Linux') {

        // Initialize Variables
        $unique           = array();
        $www_unique_count = 0;
        $www_total_count  = 0;
        $proc_count       = 0;
        $display_www      = false;

        // Check if 'exec()' is enabled
        if (function_exists('exec')) {
            $display_www = true;

            // Get HTTP connections
            @exec('netstat -an | egrep \':80|:443\' | awk \'{print $5}\' | grep -v \':::\*\' |  grep -v \'0.0.0.0\'', $results);
            foreach ($results as $result) {
                $array = explode(':', $result);
                ++$www_total_count;

                if (preg_match('/^::/', $result)) {
                    $ipaddr = $array[3];
                } else {
                    $ipaddr = $array[0];
                }

                if (!in_array($ipaddr, $unique)) {
                    $unique[] = $ipaddr;
                    ++$www_unique_count;
                }
            }
            unset($results);
        }

        // Get Server Load
        $loadavg = explode(' ', file_get_contents('/proc/loadavg'));
        $loadavg = "{$loadavg[0]} {$loadavg[1]} {$loadavg[2]}";

        // Get Disk Utilization
        $disktotal = disk_total_space('/');
        $diskfree  = disk_free_space('/');
        $diskuse   = round(100 - (($diskfree / $disktotal) * 100)) . '%';
        $uptime    = trim(file_get_contents('/proc/uptime'));
        // Get server uptime
        $uptime = (str_replace(' ', '', $uptime));
        $uptime = floor(preg_replace('/\.[0-9]+/', '', $uptime) / 86400);

        // Get kernel version
        $kernel = explode(' ', file_get_contents('/proc/version'));
        $kernel = $kernel[2];

        // Get number of processes
        $dh = opendir('/proc');
        while ($dir = readdir($dh)) {
            if (is_dir('/proc/' . $dir)) {
                if (preg_match('/^[0-9]+$/', $dir)) {
                    ++$proc_count;
                }
            }
        }

        // Get memory usage
        foreach (file('/proc/meminfo') as $result) {
            $array = explode(':', str_replace(' ', '', $result));
            $value = preg_replace('/kb/i', '', $array[1]);
            if (preg_match('/^MemTotal/', $result)) {
                $totalmem = trim($value);
            } elseif (preg_match('/^MemFree/', $result)) {
                $freemem = trim($value);
            } elseif (preg_match('/^Buffers/', $result)) {
                $buffers = trim($value);
            } elseif (preg_match('/^Cached/', $result)) {
                $cached = trim($value);
            }
        }

        $freemem = ($freemem + $buffers + $cached);
        $usedmem = round(100 - (($freemem / $totalmem) * 100)) . '%';

        $resultData['totalProcess']      = $proc_count;
        $resultData['kernelVersion']     = $kernel;
        $resultData['uptime']            = $uptime;
        $resultData['loadAverage']       = $loadavg;
        $resultData['diskUse']           = $diskuse;
        $resultData['memoryUtilization'] = $usedmem;

        $resultData['uniqueConnections'] = $display_www ? $www_unique_count : 0;
        $resultData['totalConnections']  = $display_www ? $www_total_count : 0;
    } else {
        $resultData['totalProcess']      = 0;
        $resultData['kernelVersion']     = 0;
        $resultData['uptime']            = 0;
        $resultData['loadAverage']       = 0;
        $resultData['diskUse']           = 0;
        $resultData['memoryUtilization'] = 0;

        $resultData['uniqueConnections'] = 0;
        $resultData['totalConnections']  = 0;
    }
    return $resultData;
}
