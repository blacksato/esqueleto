<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param Login $event
     */
    public function handle(Login $event)
    {
        $user = $event->user;
        $user->last_login_at = getDateSQL();
        $user->last_login_ip = request()->ip();
        $user->save();
    }
}
