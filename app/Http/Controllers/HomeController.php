<?php

namespace App\Http\Controllers;

use Route;
use Illuminate\Http\Request;
use App\Core\Repositories\SecurityRPY;
use Hash;
use Storage;
use File;
use Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function getRouter(Request $request)
    {
        if ($request->router != '') {
            if (Route::has($request->router)) {
                (new SecurityRPY())->forStoreNavigationUser($request->router, $request->ip(), currentUser());

                return redirect()->route($request->router);
            } else {
                abort(404);
            }
        } else {
            return redirect()->route('home');
        }
    }


    public function showChangePasswordForm()
    {
        return view('auth.change-password');
    }

    public function changePassword(Request $request)
    {
        $user = currentUser();
        if (!(Hash::check($request->get('current-password'), $user->password))) {
            return redirect()->back()->withErrors([__('auth.old-current-password')]);
        }
        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            return redirect()->back()->withErrors(__('auth.new-password-old'));
        }
        $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        $user->password = ($request->get('new-password'));
        $user->save();
        session()->flash('status', __('Password Change'));
        return redirect()->route('home');
    }

    public function showChangePictureForm()
    {
        return view('admin.users.change-picture')->with(['user' => currentUser()]);
    }

    public function changePicture(Request $request)
    {
        $this->validate($request, ['documentoFoto' => 'required']);
        $user = currentUser();
        $objFile = $request->file('documentoFoto');
        $ARCHIVO_FOTO = '';
        if ($objFile != null) {
            $extension = $objFile->getClientOriginalExtension();
            try {
                $ARCHIVO_FOTO = 'IMG-' . $user->cedula . '.' . $extension;
                Storage::disk('ftp')->delete('MODULOS/SIP-UG/' . $ARCHIVO_FOTO);
                Storage::disk('ftp')->put('MODULOS/SIP-UG/' . $ARCHIVO_FOTO, File::get($objFile));
            } catch (\Exception $ex) {
                throw new \Exception("Problemas con el servidor de archivos no se puede guardar el registro documentoFoto " . $ex->getMessage());
            }
            $user->avatar = route('view-avatar', $ARCHIVO_FOTO);
        }
        $user->save();
        session()->flash('status', __('Proccess OK'));
        return redirect()->route('changePicture');
    }
}
