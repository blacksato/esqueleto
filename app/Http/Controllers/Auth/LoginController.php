<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Session;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //you need add this function
    protected function credentials(Request $request)
    {
        $data = $request->only($this->username(), 'password');
        $data['status'] = 'A';
        return $data;
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        if (Auth::guard('admin')->check()) {
            $request->session()->invalidate();
        }
        return redirect($this->redirectTo);
    }


    protected function authenticated(Request $request, $user)
    {
        if ($user->session_id) {
            Session::getHandler()->destroy($user->session_id);
        }
        $user->session_id =session()->getId();
        $user->save();
        return redirect()->intended($this->redirectPath());
    }
}
