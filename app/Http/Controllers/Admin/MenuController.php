<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Core\Eloquent\Security\Menu;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\MenuRequest;
use Facades\App\Core\Contracts\TableRepository;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->abilityCRUD('menu');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.menu.index')->with(['filter' => $request->filter,
        'menus' => TableRepository::forMenus($request->filter), ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.menu.create')->with(['menus' => Menu::pluck('name', 'id')->toArray()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        Menu::create($request->validated());
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('menus.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Core\Eloquent\Security\Menu $menu
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        return view('admin.menu.edit')->with(['menu' => $menu, 'menus' => Menu::pluck('name', 'id')->toArray()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request         $request
     * @param \App\Core\Eloquent\Security\Menu $menu
     *
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $request, Menu $menu)
    {
        $menu->fill($request->validated());
        $menu->save();
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('menus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Core\Eloquent\Security\Menu $menu
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('menus.index');
    }
}
