<?php

namespace App\Http\Controllers\Admin;

use DB;
use Bouncer;
use Illuminate\Http\Request;
use Silber\Bouncer\Database\Role;
use App\Core\Eloquent\Security\Menu;
use App\Http\Controllers\Controller;
use Silber\Bouncer\Database\Ability;
use App\Core\Repositories\SecurityRPY;
use App\Core\Eloquent\Security\RoleMenu;
use App\Http\Requests\Admin\RoleRequest;

class RolesController extends Controller
{
    private $objSecurityRPY;

    public function __construct(SecurityRPY $objSecurityRPY)
    {
        $this->abilityCRUD('role', true);
        $this->objSecurityRPY = $objSecurityRPY;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.roles.index')->with([
            'filter' => $request->filter,
            'roles' => Bouncer::role()->where('name', 'like', "%{$request->filter}%")->get()->paginateCollection(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\Admin\RoleRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        Role::create($request->validated());
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('roles.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('admin.roles.edit')->with(['role' => $role]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        Role::where('id', $id)
            ->update($request->validated());
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $role
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('roles.index');
    }

    /**
     * Show Roles with menus.
     *
     * @param Role $role
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //(auth()->user()->assign('administrador'));

        return view('admin.roles.role-menu-ability', compact('role'))->with([
            'abilities' => Bouncer::ability()->get(),
            'menus' => Menu::menus([$role->id]),
            'abilityRol' => $this->objSecurityRPY->permissionByRol($role->id)->pluck('ability_id')->toArray(),
        ]);
    }

    public function menu(Request $request, Role $role)
    {
        $this->validate($request, ['menu.*' => 'required|exists:menus,id', 'menu' => 'required']);
        DB::transaction(function () use (&$role, &$request) {
            RoleMenu::where('role_id', '=', $role->id)->delete();
            $data = [];
            foreach ($request->menu as $menu) {
                $data[] = ['menu_id' => $menu, 'role_id' => $role->id, 'created_at' => getDateSQL(), 'updated_at' => getDateSQL()];
            }
            RoleMenu::insert($data);
        });

        session()->flash('status', __('Proccess OK'));

        return redirect()->route('roles.index');
    }

    public function abilities(Request $request, Role $role)
    {
        $this->validate($request, [
            'ability.*' => 'required|exists:bouncer_abilities,id',
        ]);
        DB::transaction(function () use (&$role, &$request) {
            $this->objSecurityRPY->permissionByRol($role->id)->delete();
            Bouncer::allow($role)->to($request->ability);
        });

        session()->flash('status', __('Proccess OK'));

        return redirect()->route('roles.index');
    }
}
