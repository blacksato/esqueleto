<?php

namespace App\Http\Controllers\Admin;

use Bouncer;
use Illuminate\Http\Request;
use Silber\Bouncer\Database\Ability;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AbilityRequest;

class AbilitiesController extends Controller
{
    public function __construct()
    {
        $this->abilityCRUD('ability');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.abilities.index')->with(['filter' => $request->filter,
        'abilities' => Bouncer::ability()->where('name', 'like', "%{$request->filter}%")->get()->paginateCollection(), ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.abilities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\Admin\AbilityRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AbilityRequest $request)
    {
        Ability::create($request->validated());
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('abilities.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Ability $ability)
    {
        return view('admin.abilities.edit')->with(['ability' => $ability]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(AbilityRequest $request, $id)
    {
        Ability::where('id', $id)
        ->update($request->validated());
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('abilities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Ability $ability
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ability $ability)
    {
        $ability->delete();
        session()->flash('status', __('Proccess OK'));

        return redirect()->route('abilities.index');
    }
}
