<?php

namespace App\Http\Composers;

use Illuminate\View\View;
use App\Core\Eloquent\Slide;

class SlidersComposer
{
    public function compose(View $view)
    {
        $view->slidesBanners = Slide::orderBy('position')->get();
    }
}
