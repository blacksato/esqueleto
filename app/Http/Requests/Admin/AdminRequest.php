<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return autorizePutPost(request()->method(), 'admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'role' => 'required|exists:bouncer_roles,id',
            ];
        switch (request()->method()) :

            case 'POST':
            $rules['password'] = 'required|string|min:6';
        $rules['email'] = 'required|string|email|max:255';
        break;

        case 'PUT':
        case 'PATCH':
        $rules['email'] = "required|string|email|max:255|unique:admins,email,{$this->admin->id}";
        break;

        endswitch;

        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => __('labels.name'),
            'email' => __('labels.email'),
            'password' => __('labels.password'),
            'role' => __('labels.role'),
        ];
    }
}
