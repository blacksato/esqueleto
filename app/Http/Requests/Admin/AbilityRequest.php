<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AbilityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return autorizePutPost(request()->method(), 'ability');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|regex:/(^([a-zA-Z][a-zA-z\-]+)?$)/u',
            'title' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('labels.name'),
            'title' => __('labels.description'),
        ];
    }
}
