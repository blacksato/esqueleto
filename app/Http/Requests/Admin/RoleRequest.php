<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return autorizePutPost(request()->method(), 'role');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|alpha',
            'title' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('labels.name'),
            'title' => __('labels.description'),
        ];
    }
}
