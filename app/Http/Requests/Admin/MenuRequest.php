<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
       return autorizePutPost(request()->method(),'menu');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'route' => 'required_with:parent',
            'parent' => 'required_if:parent,numeric',
            'order' => 'required|numeric',
            'icon' => 'required_without:parent',
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('labels.name'),
            'route' => __('labels.route'),
            'parent' => __('labels.parent'),
            'order' => __('labels.order'),
            'icon' => __('labels.icon'),
        ];
    }
}
