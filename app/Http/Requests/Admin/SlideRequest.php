<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return autorizePutPost(request()->method(), 'slide');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'position' => 'required|integer', 'url' => 'required|url',
        ];

        switch (request()->method()) :

            case 'POST':
            $rules['vpath'] = 'required|image';
            break;

        case 'PUT':
        case 'PATCH':
            $rules['vpath'] = 'image';
            break;

        endswitch;

        return $rules;
    }

    public function attributes()
    {
        return [
            'vpath' => __('labels.image'),
            'position' => __('labels.order'),
            'url' => __('labels.url'),
        ];
    }
}
