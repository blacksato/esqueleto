<?php

namespace App\Providers;

use Bouncer;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use App\Http\Composers\MenuComposer;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use App\Http\Composers\SlidersComposer;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\LengthAwarePaginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Carbon::setLocale(config('app.locale'));
        Carbon::setToStringFormat(getFormatDateSQL(true, true));
        Request::macro('isAdmin', function () {
            return $this->segment('1') == 'core-admin';
        });

        Bouncer::tables([
            'abilities' => 'bouncer_abilities',
            'permissions' => 'bouncer_permissions',
            'roles' => 'bouncer_roles',
            'assigned_roles' => 'bouncer_assigned_roles',
        ]);

        if (!Collection::hasMacro('paginateCollection')) {
            Collection::macro('paginateCollection',
                function ($perPage = 15, $page = null, $options = []) {
                    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

                    return (new LengthAwarePaginator(
                    $this->forPage($page, $perPage), $this->count(), $perPage, $page, $options))
                    ->withPath('');
                });
        }
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        require_once __DIR__.'/../Helpers/helpers.php';
        $this->registerViewComposer();
    }

    public function registerViewComposer()
    {
        view()->composer('partials.slidesBanner', SlidersComposer::class);
        view()->composer('partials.menu', MenuComposer::class);

        view()->composer('*', function ($view) {
            $view->with('infoServer', getInfoServer());
            $view->with('logoutAction', route(Request::isAdmin() ? 'admin.logout' : 'logout'));

        });
    }
}
