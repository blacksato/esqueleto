<?php

// Localization
Route::get('/js/lang.js', function () {
    $strings = Cache::rememberForever('lang.js', function () {
        $lang = config('app.locale');

        $files = glob(resource_path('lang/'.$lang.'/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name = basename($file, '.php');
            $strings[$name] = require $file;
        }

        return $strings;
    });

    header('Content-Type: text/javascript');
    echo 'window.i18n = '.json_encode($strings).';';
    exit();
})->name('assets.lang');

Route::get('/public/{file}', function ($file) {
    return Storage::response("public/$file");
});

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/photo/profile/{file}', function ($file = null) {
        $fileContent = Storage::disk('ftp')->get("MODULOS/SIP-UG/$file");
    return Response::make($fileContent, '200');
})->name('view-avatar');