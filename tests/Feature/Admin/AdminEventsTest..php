<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminEventsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admins_can_visit_the_admin_events_page()
    {
        $this->actingAsAdmin()
            ->get(route('admin_events'))
            ->assertSee('EVENTOS')
            ->assertStatus(200);
    }

    /** @test */
    public function non_admin_users_cannot_visit_the_admin_events_page()
    {
        $this->actingAsUser()
            ->get(route('admin_events'))
            ->assertStatus(302)
            ->assertRedirect('core-admin/login');
    }

    /** @test */
    public function guest_cannot_visit_the_admin_events_page()
    {
        $this->get(route('admin_events'))
            ->assertStatus(302)->assertRedirect('core-admin/login');
    }
}
