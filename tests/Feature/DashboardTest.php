<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DashboardTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_shows_the_dashboard_page_to_authenticated_users()
    {
        $this->actingAsUser()
            ->get(route('home'))
            ->assertSee('Dashboard')
            ->assertStatus(200);
    }

    /** @test */
    public function it_shows_the_dashboard_page_to_admins()
    {
        $this->actingAsAdmin()
             ->get(route('home'))
             ->assertSee('Dashboard')
             ->assertStatus(200);
    }

    /** @test */
    public function it_redirects_guest_users_to_the_login_page()
    {
        $this->get(route('home'))
            ->assertStatus(302)
            ->assertRedirect('login');
    }
}
