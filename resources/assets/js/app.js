
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import store from './store'

import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';

import ElementUI from 'element-ui'

import localeElementUIES from 'element-ui/lib/locale/lang/es'
import localeElementUIEN from 'element-ui/lib/locale/lang/en'

import vSelect from 'vue-select'

import 'element-ui/lib/theme-chalk/index.css'

if(window.locale=='es'){
    Vue.use(ElementUI, { localeElementUIES });
}else{
    Vue.use(ElementUI, { localeElementUIEN });
}
Vue.use(VueInternationalization);


const i18n = new VueInternationalization({
    locale: window.locale,
    messages: Locale
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs. miriam
 */



Vue.component('contact-form', require('./components/forms/FormContact.vue'));
Vue.component('contact-form-admin', require('./components/forms/FormContactAdmin.vue'));
Vue.component('notification', require('./components/singles/SearchNotification.vue'));
Vue.component('v-select', vSelect);

$(".alertFooter").fadeTo(2000, 2000).slideUp(800, function(){
    $(".alertFooter").slideUp(800);
});



const app = new Vue({
    el: '#app',
    i18n,
    store,
    methods:{
        deleteAlert(event) {
            let _self=this;
            this.$confirm("Estás seguro que desea eliminar el registro?", 'Atención', {
                confirmButtonText: 'SI',
                cancelButtonText: 'NO',
                type: 'warning'
              }).then(() => {
                event.target.submit();
              }).catch(() => { 
              });
        },
        actionAlert(event){
            let _self=this;
            this.$confirm("Estás seguro que deseas realizar esta acción sobre el registro?", 'Atención', {
                confirmButtonText: 'SI',
                cancelButtonText: 'NO',
                type: 'warning'
              }).then(() => {
                event.target.submit();
              }).catch(() => { 
              });
        }
    }
});
