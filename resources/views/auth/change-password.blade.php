@extends('layouts.app')
@section('titleOption',__("labels.change-password"))
@section('content')
              <div class="card card-outline-info">
                <div class="card-header">
                   <h4 class="m-b-0 text-white">@lang("labels.change-password")</h4>
                </div>
                {!! Form::open(['route'=>'changePassword','class'=>'login-form']) !!}
                <div class="card-body">
                    {!! Field::password('current-password',['required'=>true,'label'=>__('labels.current-password')])!!}
                    {!! Field::password('new-password',['required'=>true,'label'=>__('labels.new-password')])!!}
                    {!! Field::password('new-password_confirmation',['required'=>true,'label'=>__('labels.new-password-confirm')])!!}
                    <div class="row text-center m-t-20">
                        <div class="col-lg-6 col-md-6 m-t-20">
                                <button type="submit" class="btn btn-info waves-effect waves-light  btn-block"> 
                                    <i class="fa fa-check"></i> @lang('buttons.update')</button>
                        </div>  
                        <div class="col-lg-6 col-md-6 m-t-20">
                            <a href='{{route("home")}}'  class="btn btn-danger waves-effect waves-light text-white  btn-block">
                                <i class="fa fa-ban"></i> @lang('buttons.cancel')</a>
                        </div>
                    </div>    
                </div>
                {!! Form::close() !!}
            </div>

@endsection