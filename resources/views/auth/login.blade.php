@extends('layouts.authenticable')

@section('content')


                <div class="card">
                <div class="card-body">
                        <form class="login-form" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="text-center mb-3">
                                    <i class="fa fa-user fa-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                    <h5 class="mb-0">{{__('InputSystem')}}</h5>
                                    <span class="d-block text-muted">{{ __('Access',['name'=>'usuarios']) }}</span>
                                </div>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <input required id="email" type="email"  name="email" value="{{ old('email') }}" placeholder="{{__('E-Mail Address') }}"
                                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"> 
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                            @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                        <div class="col-xs-12">
                                            <input  placeholder="{{ __('Password') }}"
                                            id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required> 
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="col-md-12">
                                                <strong class="d-block text-gray-dark">
                                                        <label class="text-success">
                                                             <input id="checkbox-signup" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                             <label style="top:3px" for="checkbox-signup" class="check-box"></label>&nbsp; {{__('Remember Me')}}
                                                         </label>
                                                        </strong>

                                          
                                            <a  href="{{ route('password.request') }}" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> {{ __('Forgot Your Password?') }}</a> 
                                        </div>
                                </div>
                                <div class="form-group text-center m-t-20">
                                        <div class="col-xs-12">
                                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Login', ['name'=>'Admin']) }}</button>
                                        </div>
                                </div>
                                 <p>{{__('FooterPage',['year'=>date('Y')])}}</p>
                            </form>
                </div> 
             </div>
@endsection