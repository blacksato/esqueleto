@extends('layouts.authenticable')

@section('content')

            <div class=" card">
                <div class="card-body">
                        <form class="login-form" method="POST" action="{{ route('password.request') }}">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="text-center mb-3">
                                    <i class="fa fa-key fa-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                    <h5 class="mb-0">{{__('ActionSystem')}}</h5>
                                    <span class="d-block text-muted">{{ __('Reset Password') }}</span>
                                </div>

                               
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <input required id="email" type="email"  name="email" value="{{ old('email') }}" placeholder="{{__('E-Mail Address') }}"
                                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"> 
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                            @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                        <div class="col-xs-12">
                                            <input  placeholder="{{ __('Password') }}"
                                            id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required> 
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="col-xs-12">
                                            <input  placeholder="{{  __('Confirm Password') }}"
                                            id="password_confirmation" type="password" class="form-control" name="password_confirmation" required> 
                                           
                                        </div>
                                </div>
                               
                               
                                <div class="form-group text-center m-t-20">
                                        <div class="col-xs-12">
                                            <button class="btn btn-info btn-lg btn-block waves-effect waves-light" type="submit"> 
                                                    {{ __('Reset Password') }}</button>
                                        </div>
                                </div>
                                 <p>{{__('FooterPage',['year'=>date('Y')])}}</p>
                            </form>
                </div> 
             
            </div>

@endsection