@extends('layouts.app')
@section('titleOption',__('Readme'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
                <div class="col-md-8 col-md-offset-2">

                        <div class="card">
                                <img class="card-img-top" src="{{asset('images/user_bg3.jpg')}}" style="height: 300px">
                                <div class="card-body little-profile text-center">
                                    <div class="pro-img"><img src="{{asset('/images/info.png')}}" alt="user"></div>
                                    <h4 class="m-b-0">@lang('MetaDescription')</h4>
                                    <p>@lang('FooterPage',['year'=>date('Y')])</p>
                                    <a href="{{route('home')}}" class="m-t-10 waves-effect waves-dark btn btn-danger btn-md btn-rounded">@lang('buttons.home')</a>
                                    <div class="row text-center m-t-20">
                                            <div class="col-sm-4 border-right">
                                                    <div class="description-block">
                                                        <span>@lang('LegendSystem')</span>
                                                           <br/><hr/>
                                                        <span class="description-header">@lang('Version')</span>
                                                        <br/>
                                                        <span class="description-header">@lang('Tecnology')</span>
                                                        <br/>
                                                    </div>
                                                </div>
                                                <div class="col-sm-8 ">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3986.9239132641487!2d-79.89985358560182!3d-2.1825647378752286!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x902d727815ad5a9d%3A0x1d61939b42d442f!2sUniversidad+de+Guayaquil+(UG)!5e0!3m2!1ses!2sec!4v1494868206747" width="100%" height="280" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                </div>
                                    </div>
                                </div>
                            </div>
                    </div>
    </div>
</div>
@endsection
