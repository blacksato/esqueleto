@extends('layouts.app')
@section('titleOption',__('options.notifications'))

@section('content')
<div class="container">
        {!! $notifications->render() !!}
    <div class="col-lg-12">
                     
        <h5 class="border-bottom border-gray pb-2 mb-0 text-danger">@lang('options.notifications')</h5>
        <br/>
         @foreach ($notifications as $notification)  


<div class="card">
                            

                            <div class="card-body">
                                <ul class="media-list">
                                    <li class="media">
                                        <div class="mr-3">

                                            @if ($notification->is_new) 
                                                <a data-popup="tooltip" data-original-title="@lang('notifications.check')" href="{{route('check-notification', $notification->id)}}" class="btn waves-effect waves-light btn-sm btn-info mr-2 ">
                                                 <i class="fas fa-eye"></i>
                                                </a>
                                            @else 
                                                <a data-popup="tooltip" data-original-title="@lang('notifications.uncheck') " href="{{route('uncheck-notification', $notification->id)}}" class="btn waves-effect waves-light btn-sm btn-danger mr-2 "><i class="fas fa-eye-slash"></i></a>
                                            @endif

                                        </div>

                                        <div class="media-body">
                                            <h6 class="media-title"> <strong> <a class="@if ($notification->is_new) text-danger @else text-info @endif" href="{{$notification->redirect_url}}" > {{ $notification->title }}</a></strong>&nbsp;
                                                <code>{{ $notification->created_at->diffForHumans()}}</code></h6>
                                            {{ $notification->description }}
                                        </div>
                                    </li>

                                   
                                  
                                </ul>
                            </div>
                        </div>
        @endforeach
  
    </div>
</div>
@endsection
