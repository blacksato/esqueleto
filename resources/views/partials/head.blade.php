<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{asset('images/32x32.png')}}" sizes="32x32">
    <link rel="icon" href="{{asset('images/192x192.png')}}" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="{{asset('images/180x180.png')}}">
    <meta name="msapplication-TileImage" content="{{asset('images/270x270.png')}}">
    <meta name="description" content="{{__('MetaDescription')}}">
    <meta name="author" content="{{__('MetaAuthor')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/blue.css') }}" rel="stylesheet">
    @yield('cssCustom')
    @yield('jsCustom')
    <script>
        window.locale="{{ app()->getLocale() }}"
    </script>