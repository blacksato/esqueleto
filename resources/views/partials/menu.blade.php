                     <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user-material">
                    <div class="sidebar-user-material-body">
                        <div class="card-body text-center">
                            <a href="#">
                                <img onerror="this.onerror=null;this.src='{{asset('images/user.png')}}';"
                        src="{{currentUser()->avatar}}"  class="img-fluid rounded-circle shadow-1 mb-3" width="80" height="80" alt="">
                            </a>
                            <h6 class="mb-0 text-white text-shadow-dark">{{currentUser()->name}}</h6>
                            <span class="font-size-sm text-white text-shadow-dark">{{currentUser()->email}}</span>
                        </div>

                        <div class="sidebar-user-material-footer">
                            <a href="#user-nav" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle" data-toggle="collapse"><span>Acciones</span></a>
                        </div>
                    </div>

                    <div class="collapse" id="user-nav">
                        <ul class="nav nav-sidebar">
                            <li class="nav-item">
                                  <a class="nav-link" href="{{ route('changePicture') }}"><i class="fa fa-image"></i> <span>@lang('labels.change-picture')</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('changePassword') }}"><i class="ti-key"></i> <span>@lang('labels.change-password')</span></a>
                            </li>
                            <li class="nav-item">
                                 <a href="{{route('readme')}}" class="nav-link" ><i class="mdi mdi-help"></i><span> @lang('Readme')</span></a>
                             </li>
                            <li class="nav-item">
                            <a href="{{route('getAllNotification')}}" class="nav-link">
                            <i class="mdi mdi-message"></i><span>@lang('notifications.get-all')</span></a>
                            </li>

                            <li class="nav-item">
                                 <a href="#"  onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                    class="nav-link"><i class="fa fa-power-off"></i><span> @lang('Logout')</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /user menu -->
                <!-- Main navigation -->
                <div class="card card-sidebar-mobile">

                    <ul class="nav nav-sidebar" data-nav-type="accordion">

                        @foreach ($menus as $key => $item)
                        @if ($item['parent'] <> null)
                            @break
                        @endif
                        @include('partials.menu-item', ['item' => $item])
                        @endforeach  
                    </ul>
                </div>
                <!-- /main navigation -->

            </div>
            <!-- /sidebar content -->
