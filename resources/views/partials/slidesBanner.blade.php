<h4 class="m-b-20">@lang('News')</h4>
<div id="slidesControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        
        @foreach($slidesBanners as $key=> $slide)
        <div class="carousel-item @if($key==0) active @endif">
            <div class="container"> <img class="d-block img-fluid" src="/public/{{$slide->vpath}}" alt="@lang('labels.not-image')"></div>
        </div>
        @endforeach

    </div>
    <a class="carousel-control-prev" href="#slidesControls" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"  data-popup="tooltip"  title="" data-original-title="@lang('labels.preview-image')" ></span> <span class="sr-only">@lang('labels.preview-image')</span> </a>
    <a class="carousel-control-next" href="#slidesControls" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"  data-popup="tooltip"   title="" data-original-title="@lang('labels.next-image')" ></span> <span class="sr-only">@lang('labels.next-image')</span> </a>
</div>