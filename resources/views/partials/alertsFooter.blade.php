@if ($errors->any())
<div class="alertFooter alertFooter-danger">
   <span>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    </span>
</div>
@endif

@if (session('status'))
<div class="alertFooter alertFooter-info">
    <span>
     <ul>
             <li> {{ session('status') }}</li>
     </ul>
     </span>
</div>

@endif