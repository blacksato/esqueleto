
@if ($item['submenu'] == [])
         <li class="nav-item">
            <a href="{{route('master',$item['route'])}}" class="nav-link waves-effect waves-dark"><i class="{{$item['icons']}}"></i>
                <span> {{ $item['name'] }}</span> </a>
        </li>
        @else
    
        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link has-arrow waves-effect waves-dark" aria-expanded="false">
                    <i class="{{$item['icons']}}"></i> <span>{{ $item['name'] }} </span>
                    </a>
            <ul aria-expanded="false" class="nav nav-group-sub" data-submenu-title="{{ $item['name'] }}">
                @foreach ($item['submenu'] as $submenu)
                    @if ($submenu['submenu'] == [])
                        <li  class="nav-item">
                            <a class="nav-link" href="{{route('master',$submenu['route'])}}">{{ $submenu['name'] }} </a></li>
                    @else
                        @include('partials.menu-item', [ 'item' => $submenu ])
                    @endif
                @endforeach
            </ul>
        </li>
@endif

