
        <a class="navbar-nav-link dropdown-toggle" href="" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false"> <i class="icon-pulse2 mr-2"></i>
             <span class="notify" > <span class="heartbit"></span> <span class="point"></span>   </span>@lang('Notificaciones')
            @if((currentUser()->unreadNotifications->count() > 0))
          
           @endif
          
        </a>
        @if(currentUser()->notifications->count()>0)
                   <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                        <div class="dropdown-content-header">
                            <span class="font-size-sm line-height-sm text-uppercase font-weight-semibold">{{ __('notifications.recieve')}}</span>
                            <a href="#" class="text-default"><i class="icon-search4 font-size-base"></i></a>
                        </div>

                        <div class="dropdown-content-body dropdown-scrollable">
                            <ul class="media-list">
                                @foreach( currentUser()->notifications->take(4) as $notification)
                                <li class="media">
                                    <div class="mr-3">
                                        <a href="{{ $notification->url }}" class="btn bg-success-400 rounded-round btn-icon"><i class="fa fa-link"></i></a>
                                    </div>

                                    <div class="media-body"  @if($notification->is_new) style="font-weight:bold" @endif>
                                        <a href="{{ $notification->url }}">{{ $notification->title }}</a>&nbsp;{{ strMaskData($notification->description) }}
                                        <div class="font-size-sm text-muted mt-1">{{ $notification->created_at->diffForHumans() }}</div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="dropdown-content-footer bg-light">
                             <a class="font-size-sm line-height-sm text-uppercase font-weight-semibold text-grey mr-auto" href="{{route('getAllNotification')}}"> <strong>{{ __('notifications.get-all')}}</strong> <i class="fa fa-angle-right"></i> </a>
                        </div>
                    </div>
        @endif

