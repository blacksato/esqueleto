<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('partials.head')
</head>
<body>
    <div id="app">
        <main>
            <div class="preloader">
                <svg class="circular" viewBox="25 25 50 50">
                  <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> 
                </svg>
            </div> 

            <div class="navbar navbar-expand-md navbar-dark bg-indigo navbar-static">
                <div class="navbar-brand">
                    <a href="{{route('home')}}" class="d-inline-block">
                        <img src="{{asset('images/logo.png')}}" alt="@lang('options.home')">

                    </a>
                </div>

                <div class="d-md-none">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                        <i class="icon-tree5"></i>
                    </button>
                    <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                        <i class="icon-paragraph-justify3"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                                <i class="icon-paragraph-justify3"></i>
                            </a>
                        </li>
                    </ul>

                    <span class="navbar-text ml-md-3">
                        <span class="badge badge-mark border-orange-300 mr-2"></span>
                        {{config('app.name')}}
                    </span>

                    <ul class="navbar-nav ml-md-auto">
                        <li class="nav-item dropdown">
                            <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-make-group mr-2"></i>
                                @lang('ContactPosgradoForm')
                            </a>

                            <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                                <div class="dropdown-content-body p-2">
                                    <div class="col-lg-12">
                                         @admin
                                        <contact-form-admin select="{{route('search-user')}}" route="{{route('sendNotificationAlert')}}"></contact-form-admin>
                                        @else
                                        <contact-form route="{{route('sendNotificationAlert')}}"></contact-form>
                                        @endadmin
                                    
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-sphere mr-2"></i>
                                @lang('News')
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                                <div class="dropdown-content-body p-2">
                                    <div class="col-lg-12">
                                        @include('partials.slidesBanner')
                                    
                                    </div>
                                </div>
                            </div>
                        </li>
                        <notification route="{{route('searchNotification')}}"></notification>

                        <li class="nav-item">
                            <a href="#" onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();"
                                                             class="navbar-nav-link">
                                <i class="icon-switch2"></i>
                                <span class="d-md-none ml-2"> @lang('Logout')</span>
                            </a>

                                                            <form id="logout-form" action="{{ $logoutAction }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                            </form>

                        </li>
                    </ul>
                </div>
            </div>    


            <div class="page-content">
                <div class="sidebar sidebar-light sidebar-main sidebar-expand-md">
                    <div class="sidebar-mobile-toggler text-center">
                        <a href="#" class="sidebar-mobile-main-toggle">
                            <i class="icon-arrow-left8"></i>
                        </a>
                        <span class="font-weight-semibold">OPCIONES DEL MENU</span>
                        <a href="#" class="sidebar-mobile-expand">
                            <i class="icon-screen-full"></i>
                            <i class="icon-screen-normal"></i>
                        </a>
                    </div>
            <!-- /sidebar mobile toggler -->

                    @include('partials.menu')
           
                </div>
                <div class="content-wrapper">

                <div class="page-header page-header-light">
                    <div class="page-header-content header-elements-md-inline">
                        <div class="page-title d-flex">
                            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">@yield('titleOption')</span></h4>
                            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                        </div>
                        @yield('bar-search')
                       
                    </div>

                    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                        <div class="d-flex">
                            <div class="breadcrumb">
                                <a href="{{route('home')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> @lang('options.home')</a>
                                <span class="breadcrumb-item active">@yield('titleOption')</span>
                            </div>

                            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                        </div>

                        <div class="header-elements d-none">
                            <div class="breadcrumb justify-content-center">
                                <a href="#" class="breadcrumb-elements-item">
                                    <i class="icon-comment-discussion mr-2"></i>
                                    Support
                                </a>

                                <div class="breadcrumb-elements-item dropdown p-0">
                                    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-gear mr-2"></i>
                                        Settings
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account security</a>
                                        <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>
                                        <a href="#" class="dropdown-item"><i class="icon-accessibility"></i> Accessibility</a>
                                        <div class="dropdown-divider"></div>
                                        <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="content">

                <div class="row">
                     @yield('content')
                </div>
                </div>

                </div>
            </div>
        </main>
    </div>
    @include('partials.alertsFooter')
</body>
</html>