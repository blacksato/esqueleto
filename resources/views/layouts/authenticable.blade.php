<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('partials.head')
</head>
<body>
    <div id="app">
        <main>
           <div class="preloader">
                <svg class="circular" viewBox="25 25 50 50">
                  <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> 
                </svg>
            </div> 

            <div class="navbar navbar-expand-md navbar-dark bg-indigo navbar-static">
                <div class="navbar-brand">
                    <a href="{{route('home')}}" class="d-inline-block">
                        <img src="{{asset('/images/logo.png')}}" alt="">
                    </a>
                </div>
            </div>
            <div class="page-content">
            <div class="content-wrapper">
                <div class="content d-flex justify-content-center align-items-center ">
                    @yield('content')

                </div>
                 <div class="navbar navbar-expand-lg navbar-light">
                   <span class="navbar-text">
                      {{__('FooterPage',['year'=>getYear()])}}
                    </span>

            </div>
            
            </div>
            </div>
        </main>
    </div>
    @include('partials.alertsFooter')
</body>
</html>