<!DOCTYPE html>
<html lang="{{config('app.locale')}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{asset('/images/32x32.png')}}" sizes="32x32">
    <link rel="icon" href="{{asset('/images/192x192.png')}}" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="{{asset('/images/180x180.png')}}">
    <meta name="msapplication-TileImage" content="{{asset('/images/270x270.png')}}">



    <meta name="description" content="{{__('MetaDescription')}}">
    <meta name="author" content="{{__('MetaAuthor')}}">
    <title>{{config('app.name')}}</title>
    <link href="/css/app.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
 <div class="navbar navbar-expand-md navbar-dark bg-indigo navbar-static">
                <div class="navbar-brand">
                    <a href="{{route('home')}}" class="d-inline-block">
                        <img src="{{asset('/images/logo.png')}}" alt="">
                    </a>
                </div>
            </div>

    <div class="page-content">
        <div class="content-wrapper">
            <div class="content d-flex justify-content-center align-items-center">
                <div class="flex-fill">
                    <div class="text-center mb-3">
                        <h1 class="error-title">{{ $code }}</h1>
                        <h5>{{ $title }}</h5>
                         <p class="text-muted m-t-30 m-b-30">{{ $slot }}</p>
                          <a href="@admin {{route('admin_dashboard')}}  @else {{route('home')}} @endadmin" class="btn btn-info btn-rounded waves-effect waves-light m-b-40">{{__('ReturnMain')}}</a> 
                    </div>
                </div>
            </div>
            <div class="navbar navbar-expand-lg navbar-light">
                   <span class="navbar-text">
                      {{__('FooterPage',['year'=>getYear()])}}
                    </span>

            </div>
        </div>
    </div>
</body>
</html>

