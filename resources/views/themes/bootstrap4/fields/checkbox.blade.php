<div id="field_{{ $id }}" class="form-check">
    {!! $input !!}
    <label class="form-check-label" for="{{ $id }}">
        {{ $label }}
@if ($required)
<span {!!Html::classes([config('html.themes.required')])!!} >@lang('Required')</span>
@endif
    </label>
@foreach ($errors as $error)
    <div class="invalid-feedback">{{ $error }}</div>
@endforeach
</div>
