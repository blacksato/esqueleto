@component('layouts.errors')
    @slot('code')403
    @endslot   
    @slot('title') {{ __('errorsPage.403.title')}}
    @endslot   
    {{__('errorsPage.403.msg')}} 
@endcomponent