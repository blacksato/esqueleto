@component('layouts.errors')
    @slot('code')404
    @endslot   
    @slot('title') {{__('errorsPage.404.title')}}
    @endslot   
    {{__('errorsPage.404.msg')}} 
@endcomponent