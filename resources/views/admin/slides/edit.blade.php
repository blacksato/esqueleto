@extends('layouts.app')
@section('titleOption','Banners')
@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-10">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4>@lang('options.form',['action'=>__('options.add-edit'),'name'=>__('options.slides')])</h4>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route'=>['slides.update',$slide],'files'=>true,'method'=>'PUT']) !!}
                                    <div class="form-body">
                                         <legend class="font-weight-semibold text-uppercase font-size-sm">@lang('labels.admin',['name'=>__('options.slides')])</legend>
                                        <div class="row p-t-20">
                                           
                                            <div class="col-md-6">
                                                {!! Field::number('position',$slide->position,['label'=>__('labels.input',['name'=>__('labels.order')])])!!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Field::text('url',$slide->url,['label'=>__('labels.input',['name'=>__('labels.url')])])!!}
                                            </div>
                                            <div class="col-md-6">
                                                    {!! Field::file('vpath',['label'=>__('labels.select-image',['name'=>__('labels.file')])])!!}
                                                 </div>
                                            <div class="col-md-12">
                                                    <img class="img-fluid img-thumbnail"  
                                                    src="{{ asset('/public/'.$slide->vpath) }}" width="100%" style="height: auto"/>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-info waves-effect waves-light"> <i class="fa fa-check"></i> @lang('buttons.save')</button>
                                        <a href="{{route('slides.index')}}" class="btn btn-danger waves-effect waves-light text-white"><i class="fa fa-ban"></i> @lang('buttons.cancel')</a>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
             </div>     
    </div>
</div>
@endsection