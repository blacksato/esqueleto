@extends('layouts.app')
@section('titleOption','Banners')
@section('bar-search')
    @include('partials.search',[
      'route'=>route('slides.index'),
      'placeholder'=>__('labels.search',['name'=>__('labels.url')]),
      'filter'=>$filter
    ])
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                       

                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">@lang('options.table',['name'=>__('options.slides')])</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                    @can ('create-slide')
                                        <a href="{{route('slides.create')}}" class="list-icons-item btn btn-primary btn-rounded waves-effect waves-light btn-sm text-white">
                                            @lang('labels.add')</a>
                                    @endcan
                            </div>
                        </div>
                    </div>


                    <div class="card-body collapse show">
                        <table class="table product-overview table-responsive-stack">
                            <thead>
                                <tr>
                                    <th class="text-center">@lang('labels.image')</th>
                                    <th  class="text-center"> @lang('labels.order') </th>
                                    <th class="text-center"> @lang('labels.url') </th>
                                    <th class="text-center"> @lang('labels.actions')  </th>
                                </tr>
                            </thead>
                            <tbody>
                               @forelse($slides as $slide)
                                <tr>
                                    <td class="text-center"><img class="img-fluid img-thumbnail" style="width:204px;height:auto;" 
                                        src="{{ asset('/public/'.$slide->vpath) }}" width="100%"/></td>
                                    <td class="text-center"><span>{{$slide->position}}</span></td>
                                    <td class="text-center"> 
                                        <a data-popup="tooltip"   title="" data-original-title="{{$slide->url}}" 
                                        class="badge badge-dark" href="{{$slide->url}}" target="_blank">
                                        <span class="label label-table label-warning">@lang('labels.navigation')</span>
                                    </a></td>
                                    <td class="text-center"> 
                                        <form  action="{{route('slides.destroy',$slide->id)}}" method="POST" class="form-horizontal" @submit.prevent="deleteAlert($event)">
                                            @method('DELETE')
                                            @csrf
                                            @can ('update-slide')
                                                <a href="{{route('slides.edit',$slide->id)}}"><span class="btn waves-effect waves-light badge badge-info">@lang('labels.edit')</span></a>
                                          
                                               
                                                @endcan
                                            @can ('delete-slide')
                                                <button type="submit" class="btn waves-effect waves-light badge badge-danger"><span>@lang('labels.delete')</span></button> 
                                            @endcan
                                        </form>
                                    </td>
                                </tr>
                               @empty
                               <tr><td colspan="4">@lang('labels.notresults')</td></tr>
                               @endforelse
                            </tbody>
                        </table>
                        <div class="text-right">
                            {!! $slides->appends(['filter' => $filter])->render()!!}
                    </div>
                    </div>
                </div>

             </div>     
    </div>
</div>
@endsection