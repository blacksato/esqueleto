@extends('layouts.app')
@section('titleOption',__('options.menu-ability'))
@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-lg-6">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4>@lang('labels.admin',['name'=>__('options.menus')])</h4>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route'=>['roles.menu',$role],'method'=>'PUT']) !!}
                                        <div class="form-body">
                                                 <legend class="font-weight-semibold text-uppercase font-size-sm">@lang('options.roles'): {{$role->title}}</legend>
                                                <div class="row p-t-20">
                                                    <div class="col-md-12">
                                                            @include('partials.menu-tree',['menus'=>$menus])
                                                    </div>    
                                                </div>
                                            </div>                                            
                                        <br/>
                                        <div class="form-actions">
                                            @can ('role-menu')
                                                <button type="submit" class="btn btn-info waves-effect waves-light"> <i class="fa fa-check"></i> @lang('buttons.save')</button>
                                            @endcan
                                        <a href="{{route('roles.index')}}" class="btn btn-danger waves-effect waves-light text-white"><i class="fa fa-ban"></i> @lang('buttons.cancel')</a>
                                    </div>
                                {!! Form::close() !!}
                              
                               
                            </div>
                        </div>
             </div>     
             <div class="col-lg-10">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">@lang('labels.admin',['name'=>__('options.abilities')])</h4>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route'=>['roles.ability',$role],'method'=>'PUT']) !!}
                                    <div class="form-body">
                                            <div class="row">
                                                @foreach($abilities as $keyAbility => $ability)
                                                    <p class="col-lg-3" style="margin: 0px" >
                                                                    <label class="pointerSkill">
                                                                        <input id="ability-{{ $keyAbility }}" type="checkbox" @if(in_array($ability->id,$abilityRol)) checked @endif                                                                    
                                                                        name="ability[]" value="{{ $ability->id}}">
                                                                        <label for="ability-{{ $keyAbility }}" class="check-box"></label>&nbsp; <span class="text-small"  data-popup="tooltip"  title="" data-original-title="{{$ability->title}}"> {{$ability->name}}</span>
                                                                    </label>
                                                                  
                                                            </p>
                                                @endforeach
                                            </div>
                                        </div>   
                                    <div class="form-actions">
                                    @can ('role-ability')
                                        <button type="submit" class="btn btn-info waves-effect waves-light"> <i class="fa fa-check"></i> @lang('buttons.save')</button>
                                    @endcan
                                    <a href="{{route('roles.index')}}" class="btn btn-danger waves-effect waves-light text-white"><i class="fa fa-ban"></i> @lang('buttons.cancel')</a>
                                </div>
                            {!! Form::close() !!}
                          
                           
                        </div>
                    </div>
         </div>     
    </div>
</div>
@endsection