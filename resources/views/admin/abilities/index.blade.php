@extends('layouts.app')
@section('titleOption',__('options.abilities'))
@section('bar-search')
    @include('partials.search',[
      'route'=>route('abilities.index'),
      'placeholder'=>__('labels.search',['name'=>__('options.abilities')]),
      'filter'=>$filter
    ])
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                       


                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">@lang('options.table',['name'=>__('options.abilities')])</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                           @can ('create-ability')
                                        <a href="{{route('abilities.create')}}" class="list-icons-item btn btn-primary btn-rounded waves-effect waves-light btn-sm text-white">
                                            @lang('labels.add')</a>
                                    @endcan
                            </div>
                        </div>
                    </div>


                    <div class="card-body  collapse show">
                        <table class="table product-overview table-responsive-stack">
                            <thead>
                                <tr>
                                    <th class="text-center">@lang('labels.name')</th>
                                    <th class="text-center"> @lang('labels.description') </th>
                                    <th class="text-center"> @lang('labels.actions')  </th>
                                </tr>
                            </thead>
                            <tbody>
                               @forelse($abilities as $ability)
                                <tr>
                                    <td class="text-center">{{$ability->name}}</td>
                                    <td class="text-center">{{$ability->title}}</td>
                                    <td class="text-center"> 
                                        <form  action="{{route('abilities.destroy',$ability->id)}}" method="POST" class="form-horizontal" @submit.prevent="deleteAlert($event)">
                                            @method('DELETE')
                                            @csrf
                                            @can ('update-ability')
                                                <a href="{{route('abilities.edit',$ability->id)}}"><span class="btn waves-effect waves-light badge badge-info">@lang('labels.edit')</span></a>
                                            @endcan
                                            @can('delete-ability')
                                                <button type="submit" class="btn waves-effect waves-light badge badge-danger"><span>@lang('labels.delete')</span></button> 
                                            @endcan
                                        </form>
                                    </td>
                                </tr>
                               @empty
                               <tr><td colspan="6">@lang('labels.notresults')</td></tr>
                               @endforelse
                            </tbody>
                        </table>
                        <div class="text-right">
                            {!! $abilities->appends(['filter' => $filter])->render()!!}
                    </div>
                    </div>
                </div>

             </div>     
    </div>
</div>
@endsection