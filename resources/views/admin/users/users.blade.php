@extends('layouts.app')
@section('titleOption',__('options.user'))
@section('bar-search')
    @include('partials.search',[
      'route'=>route('user.index'),
      'placeholder'=>__('labels.search',['name'=>__('labels.name')]),
      'filter'=>$filter
    ])
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
<div class="col-md-12">
            <div class="card">
                   

                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">@lang('options.table',['name'=>__('options.user')])</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                    @can ('create-user')
                                        <a href="{{route('user.create')}}" class="btn badge-info badge-rounded waves-effect waves-light btn-sm text-white">
                                            @lang('labels.add')</a>
                                    @endcan
                            </div>
                        </div>
                    </div>



                    <div class="card-body collapse show">
                        <div class="table-responsive">
                            <table id="tableAccordion" class="table product-overview table-responsive-stack">
                                <thead>
                                    <tr>
                                            <th class="text-center">@lang('labels.image')</th>
                                            <th class="text-center">@lang('labels.name')</th>    
                                            <th  class="text-center"> @lang('labels.email') </th>
                                            <th  class="text-center"> @lang('labels.status') </th>
                                            <th class="text-center"> @lang('labels.actions')  </th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @forelse($users as $user)
                                   
                                    <tr>
                                        <td>
                                            <img onerror="this.onerror=null;this.src='{{asset('images/user.png')}}';" src="{{$user->avatar}}" alt="user" width="50">
                                        </td>
                                        <td>{{$user->name}}</td> 
                                        <td class="text-center">{{$user->email}}</td>
                                        <td class="text-center">
                                            @if($user->status=='A')
                                                <span class="label label-success font-weight-100">@lang('labels.active') </span>
                                            @else
                                                <span class="label label-danger font-weight-100">@lang('labels.inactive') </span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                        <form  action="{{route('user.destroy',$user->id)}}" id="frmDelete{{$user->id}}" method="POST" class="form-horizontal" @submit.prevent="actionAlert($event)">
                                                @method('DELETE')
                                                @csrf
                                               
                                                @can('show-user')     
                                                <a href="{{route('user.show',$user->id)}}" class="btn badge  waves-effect waves-light badge-success" 
                                                    data-popup="tooltip"  title="" data-original-title="@lang('labels.view')">
                                                    <i class="fas fa-user"></i></a>
                                                @endcan

                                            @can('role-user')    
                                            <a href="{{route('change-role-user',$user->id)}}" class="btn badge  waves-effect waves-light badge-primary" 
                                                data-popup="tooltip"  title="" data-original-title="@lang('labels.change-role')">
                                                <i class="fas fa-sitemap"></i></a>
                                            @endcan
                                            @can('password-user')
                                            <a href="{{route('change-password-user',$user->id)}}" class="btn badge  waves-effect waves-light badge-success" 
                                                data-popup="tooltip"  title="" data-original-title="@lang('labels.change-password')">
                                                <i class="fas fa-key"></i></a>
                                            @endcan  
                                            @can('update-user')
                                            <a href="{{route('user.edit',$user->id)}}" class="btn badge  waves-effect waves-light badge-primary" 
                                            data-popup="tooltip"  title="" data-original-title="@lang('labels.edit')"><i class="fas fa-edit"></i></a> 
                                            @endcan  
                                            @can ('delete-user')
                                                <button type="submit"  title="" data-popup="tooltip" 
                                                    data-original-title="@lang('labels.active-inactive-user')" class="btn waves-effect waves-light badge badge-danger">
                                                        <span><i class="fas fa-exchange-alt"></i></span>
                                                </button> 
                                            
                                            @endcan

                                        </form>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr><td colspan="5">@lang('labels.notresults')</td></tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
           


    </div></div>
</div>
@endsection