@extends('layouts.app')
@section('titleOption',__("options.$type"))
@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-4">
                    <div class="card">
                            <img class="card-img-top" style="height: 110px" src="{{asset('images/userinfo.jpg')}}" alt="Card image cap">
                            <div class="card-body little-profile text-center">
                                <div class="pro-img"><img src="{{asset('images/user.png')}}" alt="user"></div>
                               
                                {!! Form::open(['route'=>["$type.store"]]) !!}
                                <div class="text-left">
                                
                                        {!! Field::text('name',null,['label'=>__('labels.name'),
                                        'placeholder'=>__('labels.input',['name'=>__('labels.name')])])!!}
                                
                                        {!! Field::email('email',null,['label'=>__('labels.email'),
                                        'placeholder'=>__('labels.input',['name'=>__('labels.email')])])!!}
                                
                                        {!! Field::password('password',['label'=>__('labels.password'),
                                        'placeholder'=>__('labels.select',['name'=>__('labels.password')])])!!}
                                
                                        {!! Field::select('role',$roles,null,['label'=>__('labels.role'),
                                        'empty'=>__('labels.select',['name'=>__('labels.role')])])!!}
                                           
                                </div>
                                <div class="row text-center m-t-20">
                                    <div class="col-lg-6 m-t-20">
                                            <button type="submit" class="btn btn-info waves-effect waves-light  btn-block"> 
                                                <i class="fa fa-check"></i> @lang('buttons.save')</button>
                                    </div>  
                                    <div class="col-lg-6 m-t-20">
                                        <a href='{{route("$type.index")}}'  class="btn btn-danger waves-effect waves-light text-white btn-block">
                                            <i class="fa fa-ban"></i> @lang('buttons.cancel')</a>
                                    </div>

                                </div>
                                {!! Form::close() !!}
                            </div>
                    </div>       
             </div>     
    </div>
</div>
@endsection