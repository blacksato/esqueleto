@extends('layouts.app')
@section('titleOption',__("options.$type"))
@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-4">
                    <div class="card">
                            <img class="card-img-top" src="{{asset('images/user_bg3.jpg')}}" alt="Card image cap">
                            <div class="card-body little-profile text-center">
                                <div class="pro-img"><img src="{{asset('images/user.png')}}" alt="user"></div>
                               
                                <div class="text-left">
                                        <small class="text-muted">@lang('labels.name')</small>
                                        <h6>{{$user->name}}</h6>
                                       
                                        <small class="text-muted">@lang('labels.email')</small>
                                        <h6>{{$user->email}}</h6>

                                        <small class="text-muted">@lang('labels.role')</small>
                                        <h6>{{optional($user->roles()->first())->title}}</h6>

                                        <small class="text-muted">@lang('labels.created_at')</small>
                                        <h6>{{$user->created_at->diffForHumans()}}</h6>

                                        <small class="text-muted">@lang('labels.last_login')</small>
                                        <h6>{{($user->last_login_at)}}</h6>

                                        <small class="text-muted">@lang('labels.created_by')</small>
                                        <h6>{{(optional($user->createdUser)->name)}}</h6>

                                </div>
                                <div class="row text-center m-t-20">
                                    <div class="col-lg-12 m-t-20">
                                        <a href='{{route("$type.index")}}'  class="btn btn-danger waves-effect waves-light text-white btn-block">
                                            <i class="fa fa-undo"></i> @lang('buttons.return')</a>
                                    </div>
                                </div>
                            </div>
                    </div>       
             </div>     
    </div>
</div>
@endsection