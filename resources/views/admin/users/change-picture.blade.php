@extends('layouts.app')
@section('titleOption',__("options.change-picture"))
@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-4">
                    <div class="card">
                            <img class="card-img-top" src="{{asset('images/user_bg3.jpg')}}" alt="Card image cap" style="height: 140px">
                            <div class="card-body little-profile text-center">
                                <div class="pro-img"><img onerror="this.onerror=null;this.src='{{asset('images/user.png')}}';" src="{{$user->avatar}}" alt="user"></div>
                                <h3 class="m-b-0">{{$user->name}}</h3>
                              
                                {!! Form::open(['route'=>["changePicture"],'files'=>true]) !!}
                                <div class="text-left">
                                {!! Field::file('documentoFoto',['label'=>__('labels.image')])!!}
                            </div>
                                <div class="row text-center m-t-20">
                                    <div class="col-lg-6 col-md-6 m-t-20">
                                            <button type="submit" class="btn btn-info waves-effect waves-light  btn-block"> 
                                                <i class="fa fa-check"></i> @lang('buttons.save')</button>
                                    </div>  
                                    <div class="col-lg-6 col-md-6 m-t-20">
                                        <a href='{{route("home")}}'  class="btn btn-danger waves-effect waves-light text-white  btn-block">
                                            <i class="fa fa-ban"></i> @lang('buttons.cancel')</a>
                                    </div>

                                </div>
                                {!! Form::close() !!}
                            </div>
                    </div>       
             </div>     
    </div>
</div>
@endsection