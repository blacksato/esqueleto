@extends('layouts.app')
@section('titleOption',__('options.menus'))
@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-10">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 >@lang('options.form',['action'=>__('options.add-edit'),'name'=>__('options.menus')])</h4>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route'=>'menus.store','files'=>true]) !!}
                                    <div class="form-body">
                                         <legend class="font-weight-semibold text-uppercase font-size-sm">@lang('labels.admin',['name'=>__('options.menus')])</legend>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                {!! Field::text('name',['required'=>true,'label'=>__('labels.name'),'placeholder'=>__('labels.input',['name'=>__('labels.name')])])!!}
                                            </div> 
                                            <div class="col-md-6">
                                                {!! Field::text('route',['label'=>__('labels.route'),'placeholder'=>__('labels.input',['name'=>__('labels.route')])])!!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Field::text('icon',['label'=>__('labels.icon'),'placeholder'=>__('labels.input',['name'=>__('labels.icon')])])!!}
                                            </div> 
                                           
                                            <div class="col-md-6">
                                                {!! Field::number('order',['required'=>true,'label'=>__('labels.order'),'placeholder'=>__('labels.input',['name'=>__('labels.order')])])!!}
                                            </div> 
                                            <div class="col-md-6">
                                                {!! Field::select('parent',$menus,null,['label'=>__('labels.parent'),'empty'=>__('labels.select',['name'=>__('labels.parent')])])!!}
                                            </div>
                                                
                                        </div>
                                    </div>
                                
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-info waves-effect waves-light"> <i class="fa fa-check"></i> @lang('buttons.save')</button>
                                        <a href="{{route('menus.index')}}" class="btn btn-danger waves-effect waves-light text-white"><i class="fa fa-ban"></i> @lang('buttons.cancel')</a>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
             </div>     
    </div>
</div>
@endsection