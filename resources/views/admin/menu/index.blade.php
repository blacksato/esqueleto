@extends('layouts.app')
@section('titleOption',__('options.menus'))
@section('bar-search')
    @include('partials.search',[
      'route'=>route('menus.index'),
      'placeholder'=>__('labels.search',['name'=>__('labels.name')]),
      'filter'=>$filter
    ])
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    

                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">@lang('options.table',['name'=>__('options.menus')])</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                           @can ('create-menu')
                                        <a href="{{route('menus.create')}}" class="list-icons-item btn btn-primary btn-rounded waves-effect waves-light btn-sm text-white">
                                            @lang('labels.add')</a>
                                    @endcan
                            </div>
                        </div>
                    </div>




                    <div class="card-body collapse show">
                        <table class="table product-overview table-responsive-stack">
                            <thead>
                                <tr>
                                    <th class="text-center">@lang('labels.name')</th>
                                    <th class="text-center"> @lang('labels.parent') </th>
                                    <th  class="text-center"> @lang('labels.order') </th>
                                    <th class="text-center"> @lang('labels.route') </th>
                                    <th class="text-center"> @lang('labels.icon') </th>
                                    <th class="text-center"> @lang('labels.actions')  </th>
                                </tr>
                            </thead>
                            <tbody>
                               @forelse($menus as $menu)
                                <tr>
                                    <td class="text-center">{{$menu->name}}</td>
                                    <td class="text-center">{{$menu->father}}</td>
                                    <td class="text-center">{{$menu->order}}</td>
                                    <td class="text-center">{{$menu->route}}</td>
                                    <td class="text-center"><i class="{{ $menu->icon}}"></i></td>
                                    <td class="text-center"> 
                                        <form  action="{{route('menus.destroy',$menu->id)}}" method="POST" class="form-horizontal" @submit.prevent="deleteAlert($event)">
                                            @method('DELETE')
                                            @csrf
                                            @can ('update-menu')
                                                <a href="{{route('menus.edit',$menu->id)}}"><span class="btn badge waves-effect waves-light badge-info">@lang('labels.edit')</span></a>
                                            @endcan
                                            @can ('delete-menu')
                                                <button type="submit" class="btn badge waves-effect waves-light badge-danger"><span>@lang('labels.delete')</span></button> 
                                            @endcan
                                        </form>
                                    </td>
                                </tr>
                               @empty
                               <tr><td colspan="6">@lang('labels.notresults')</td></tr>
                               @endforelse
                            </tbody>
                        </table>
                        <div class="text-right">
                            {!! $menus->appends(['filter' => $filter])->render()!!}
                    </div>
                    </div>
                </div>

             </div>     
    </div>
</div>
@endsection