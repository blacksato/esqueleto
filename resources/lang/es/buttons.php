<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
 */

    'send' => 'Enviar',
    'cancel' => 'Cancelar',
    'add' => 'Agregar',
    'clean' => 'Limpiar',
    'proccess' => 'Procesar',
    'response' => 'Responder',
    'check-response' => 'Ver Respuesta',
    'save' => 'Guardar',
    'home' => 'Inicio',
    'return' => 'Regresar',
    'update' => 'Actualizar'
];
