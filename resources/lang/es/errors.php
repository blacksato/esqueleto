<?php

return [
'file-size' => 'El tamaño máximo del archivo es de 2 megas',
    'format-image' => 'El formato del archivo debe se jpg, jpeg ó png',
    'ftp' => 'Problemas con el servidor de archivos no se puede guardar el registro, intente mas tarde',
    'not-data-person' => 'Antes de Guardar esta sección debes guardar o actualizar los datos personales',
    'not-skills' => 'No tienes asignada la habilidad que permita realizar esta operación',
    'owner' => 'No estás autorizado para realizar esta operación',
    'duplicate' => 'No puedes ingresar combinaciones duplicadas comunícate con el administrador',
    
 ];
