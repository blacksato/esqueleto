<?php

return [
    'home' => 'Inicio',
    'notifications' => 'Notificaciones',
    'slides' => 'Banners',
    'menus' => 'Menús',
    'menu-ability' => 'Menús y Habilidades ',
    'roles' => 'Roles',
    'role-menus' => 'Menú por Rol',
    'abilities' => 'Habilidades',
    'add' => 'Ingreso',
    'edit' => 'Edición',
    'add-edit' => 'Ingreso/Edición',
    'form' => 'Formulario de :action de :name',
    'table' => ':Name ingresados en el sistema',
    'user' => 'Usuarios',
    'admin' => 'Administradores', 'logs' => 'Visor de Logs',
    'change-picture'=>'Cambiando Imagen de Perfil'
];
